package com.five_code.smartorder.base;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

/**
 * Created by Okta Dwi Priatna on 5/31/17.
 * okta.dwi1@gmail.com
 */

public class BaseDialogFragment extends DialogFragment {

    protected Context mContext;
    protected LayoutInflater mLayoutInflater;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mLayoutInflater = LayoutInflater.from(activity);
    }

}
