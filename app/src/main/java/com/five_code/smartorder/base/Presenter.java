package com.five_code.smartorder.base;

/**
 * Created by Okta Dwi Priatna on 5/16/17.
 * okta.dwi1@gmail.com
 */

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}
