package com.five_code.smartorder.base;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public interface MvpView {

    void onShowLoading();

    void onDismissLoading();

    void onFailed(String message);
    
}
