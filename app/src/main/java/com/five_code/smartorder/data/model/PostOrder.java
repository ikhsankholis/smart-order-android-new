package com.five_code.smartorder.data.model;

import java.util.List;

/**
 * Created by IKHSAN on 6/14/2017.
 */

public class PostOrder {

    private List<ArrayBean> array;

    public PostOrder(List<ArrayBean> array) {
        this.array = array;
    }

    public static class ArrayBean {
        private String direct_order_id;
        private String menu_id;
        private String quantity;
        private String subtotals;
        private String status;
        private String note;

        public ArrayBean(String direct_order_id, String menu_id, String quantity, String subtotals, String status, String note) {
            this.direct_order_id = direct_order_id;
            this.menu_id = menu_id;
            this.quantity = quantity;
            this.subtotals = subtotals;
            this.status = status;
            this.note = note;
        }
    }

}
