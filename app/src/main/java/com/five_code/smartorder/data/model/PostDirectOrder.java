package com.five_code.smartorder.data.model;

/**
 * Created by IKHSAN on 6/16/2017.
 */

public class PostDirectOrder {

    private String table_id;
    private String status;

    public PostDirectOrder(String table_id, String status) {
        this.table_id = table_id;
        this.status = status;
    }
}
