package com.five_code.smartorder.data.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

@AutoValue
public abstract class BaseResponse {

    public static TypeAdapter<BaseResponse> typeAdapter(Gson gson) {
        return new AutoValue_BaseResponse.GsonTypeAdapter(gson);
    }

    public abstract String message();

    public abstract boolean status();
}
