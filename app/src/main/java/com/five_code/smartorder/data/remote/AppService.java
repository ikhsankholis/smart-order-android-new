package com.five_code.smartorder.data.remote;

import com.five_code.smartorder.BuildConfig;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.data.model.OrderResponse;
import com.five_code.smartorder.data.model.PostDirectOrder;
import com.five_code.smartorder.data.model.PostOrder;
import com.five_code.smartorder.data.model.Table;
import com.five_code.smartorder.utils.MyGsonTypeAdapterFactory;
import com.five_code.smartorder.utils.RxErrorHandlingCallAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

public interface AppService {

    String ENDPOINT = "http://45.127.134.18/smart-order-api/";

    @GET("tables")
    Observable<List<Table>> getTable();

    @GET("menus")
    Observable<List<Menu>> getMenu();

    @POST("list_orders")
    Observable<OrderResponse> postOrder(@Body PostOrder postOrder);

    @POST("open_table")
    Observable<DirectOrderResponse> postDirectOrder(@Body PostDirectOrder postDirectOrder);

    /******** Helper class that sets up a new services *******/
    class Creator {

        public static AppService newAppService() {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);
            httpClientBuilder.addInterceptor(logging).build();

            httpClientBuilder.addInterceptor(new Interceptor());

            Gson gson = new GsonBuilder().registerTypeAdapterFactory(MyGsonTypeAdapterFactory.create())
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .create();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(AppService.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()))
                    .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                    .callFactory(httpClientBuilder.build())
                    .build();
            return retrofit.create(AppService.class);
        }
    }
}
