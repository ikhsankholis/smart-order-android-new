package com.five_code.smartorder.data.model;

/**
 * Created by IKHSAN on 5/26/2017.
 */

public class Table {

    private String _id;
    private String meja_no;
    private String meja_status;
    private String createdAt;
    private String lastUpdate;

    public String get_id() {
        return _id;
    }

    public String getMeja_no() {
        return meja_no;
    }

    public String getMeja_status() {
        return meja_status;
    }

    public void setMeja_status(String meja_status) {
        this.meja_status = meja_status;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
