package com.five_code.smartorder.data;

import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.data.model.OrderResponse;
import com.five_code.smartorder.data.model.PostDirectOrder;
import com.five_code.smartorder.data.model.PostOrder;
import com.five_code.smartorder.data.model.Table;
import com.five_code.smartorder.data.remote.AppService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@Singleton
public class DataManager {

    private final AppService appService;

    @Inject
    public DataManager(AppService appService) {
        this.appService = appService;
    }

    public Observable<List<Table>> getTable() {
        return appService.getTable();
    }

    public Observable<List<Menu>> getMenu() {
        return appService.getMenu();
    }

    public Observable<DirectOrderResponse> postDirectOrder(PostDirectOrder postDirectOrder) {
        return appService.postDirectOrder(postDirectOrder);
    }

    public Observable<OrderResponse> postOrder(PostOrder postOrder) {
        return appService.postOrder(postOrder);
    }
}
