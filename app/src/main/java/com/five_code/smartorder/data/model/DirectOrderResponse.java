package com.five_code.smartorder.data.model;

import java.io.Serializable;

/**
 * Created by IKHSAN on 6/16/2017.
 */

public class DirectOrderResponse {

    private String message;
    private DirectordersBean directorders;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DirectordersBean getDirectorders() {
        return directorders;
    }

    public void setDirectorders(DirectordersBean directorders) {
        this.directorders = directorders;
    }

    public static class DirectordersBean implements Serializable {

        private String table_id;
        private String status;
        private String _id;
        private String createdAt;
        private String lastUpdate;

        public String getTable_id() {
            return table_id;
        }

        public void setTable_id(String table_id) {
            this.table_id = table_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getLastUpdate() {
            return lastUpdate;
        }

        public void setLastUpdate(String lastUpdate) {
            this.lastUpdate = lastUpdate;
        }
    }
}
