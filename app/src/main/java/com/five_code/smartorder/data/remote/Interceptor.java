package com.five_code.smartorder.data.remote;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

public class Interceptor implements okhttp3.Interceptor {

    @Override public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        Request original = chain.request();

        String method = original.method();
        HttpUrl.Builder httpBuilder = original.url().newBuilder();

        // Request customization: add request headers
        Request.Builder requestBuilder = original.newBuilder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .method(method, original.body());

        //requestBuilder.addHeader("",);

        Request request = requestBuilder.url(httpBuilder.build()).build();

        response.body().close();

        return chain.proceed(request);
    }
}