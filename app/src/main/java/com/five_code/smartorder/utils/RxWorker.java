package com.five_code.smartorder.utils;

import rx.Observable;

/**
 * Created by Okta Dwi Priatna on 5/16/17.
 * okta.dwi1@gmail.com
 */

public class RxWorker {
    public static Observable<Object> doInComputation(final Runnable runnable) {
        return Observable.create(subscriber -> {
            runnable.run();
            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext("We are done bro!");
            }
        }).compose(RxUtils.applySchedulersComputation());
    }

    public static Observable<Object> doInIO(final Runnable runnable) {
        return Observable.create(subscriber -> {
            runnable.run();
            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext("We are done bro!");
            }
        }).compose(RxUtils.applyScheduler());
    }

    public static Observable<Object> doInNewThread(final Runnable runnable) {
        return Observable.create(subscriber -> {
            runnable.run();
            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext("We are done bro!");
            }
        }).compose(RxUtils.applySchedulersNewThread());
    }
}