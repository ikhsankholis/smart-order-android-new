package com.five_code.smartorder.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by Okta Dwi Priatna on 6/2/17.
 * okta.dwi1@gmail.com
 */

public class Utils {

    public static String convertNumber(long number) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        String result;
        String recentLanguage = Locale.getDefault().getDisplayLanguage();
        if (recentLanguage.equals("Bahasa Indonesia")) {
            result = kursIndonesia.format(number);
        } else {
            result = kursIndonesia.format(number).substring(0, kursIndonesia.format(number).length() - 3);
        }
        return result;
    }

}
