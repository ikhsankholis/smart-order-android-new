package com.five_code.smartorder.utils;

import com.five_code.smartorder.data.model.BaseResponse;

/**
 * Created by Okta Dwi Priatna on 5/15/17.
 * okta.dwi1@gmail.com
 */

public class AppException extends Exception {

    private final int mResponseCode;
    private final BaseResponse mBaseResponse;

    public AppException(BaseResponse mBaseResponse) {
        this.mBaseResponse = mBaseResponse;
        this.mResponseCode = 200;
    }

    public AppException(BaseResponse baseResponse, int responseCode) {
        this.mBaseResponse = baseResponse;
        this.mResponseCode = responseCode;
    }

    public BaseResponse getResponse() {
        return mBaseResponse;
    }

    public int getResponseCode() {
        return mResponseCode;
    }
}
