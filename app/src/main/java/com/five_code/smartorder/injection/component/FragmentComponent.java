package com.five_code.smartorder.injection.component;

import com.five_code.smartorder.injection.PerFragment;
import com.five_code.smartorder.injection.module.FragmentModule;
import com.five_code.smartorder.ui.cart.CartFragment;
import com.five_code.smartorder.ui.menu.MenuFragment;

import dagger.Subcomponent;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(MenuFragment menuFragment);

    void inject(CartFragment cartFragment);
}
