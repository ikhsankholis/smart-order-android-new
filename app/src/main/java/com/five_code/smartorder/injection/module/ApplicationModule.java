package com.five_code.smartorder.injection.module;

import android.app.Application;
import android.content.Context;


import com.five_code.smartorder.data.remote.AppService;
import com.five_code.smartorder.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides @Singleton
    AppService provideRibotsService() {
        return AppService.Creator.newAppService();
    }

}