package com.five_code.smartorder.injection.component;

import com.five_code.smartorder.injection.ConfigPersistent;
import com.five_code.smartorder.injection.module.ActivityModule;
import com.five_code.smartorder.injection.module.FragmentModule;

import dagger.Component;

@ConfigPersistent
@Component(dependencies = ApplicationComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);
}