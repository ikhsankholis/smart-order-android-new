package com.five_code.smartorder.injection.module;

import android.app.Activity;
import android.content.Context;

import com.five_code.smartorder.injection.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@Module
public class FragmentModule {

    private Activity mActivity;

    public FragmentModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides @ActivityContext
    Context providesContext() {
        return mActivity;
    }
}
