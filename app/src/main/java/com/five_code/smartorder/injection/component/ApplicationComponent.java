package com.five_code.smartorder.injection.component;

import android.app.Application;
import android.content.Context;

import com.five_code.smartorder.data.DataManager;
import com.five_code.smartorder.injection.ApplicationContext;
import com.five_code.smartorder.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Okta Dwi Priatna on 3/12/17.
 * okta.dwi1@gmail.com
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    DataManager dataManager();

}