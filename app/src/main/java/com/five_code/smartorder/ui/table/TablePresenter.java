package com.five_code.smartorder.ui.table;

import com.five_code.smartorder.base.BasePresenter;
import com.five_code.smartorder.data.DataManager;
import com.five_code.smartorder.data.model.PostDirectOrder;
import com.five_code.smartorder.utils.ErrorHandler;
import com.five_code.smartorder.utils.RxUtils;

import javax.inject.Inject;

/**
 * Created by IKHSAN on 6/1/2017.
 */

public class TablePresenter extends BasePresenter<TableView> {

    @Inject
    public TablePresenter(DataManager dataManager) { mDataManager = dataManager; }

    @Override
    public void attachView(TableView mvpView) { super.attachView(mvpView); }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }
    public void getTable(){
        checkViewAttached();
        RxUtils.unsubscribe(mSubscription);

        getMvpView().onShowLoading();
        mSubscription = mDataManager.getTable()
                .doOnTerminate(()-> getMvpView().onDismissLoading())
                .compose(RxUtils.applyApiCall())
                .subscribe(tablesList -> {
                    getMvpView().onSuccessTable(tablesList);
                }, throwable -> {
                    ErrorHandler.handlerErrorPresenter(getMvpView(),throwable);
                });
    }

    public void postDirectOrder(PostDirectOrder postDirectOrder){
        checkViewAttached();
        RxUtils.unsubscribe(mSubscription);

        getMvpView().onShowLoading();
        mSubscription = mDataManager.postDirectOrder(postDirectOrder)
                .doOnTerminate(()-> getMvpView().onDismissLoading())
                .compose(RxUtils.applyApiCall())
                .subscribe(directOrderResponse -> {
                    getMvpView().onSuccessDirectOrder(directOrderResponse);
                }, throwable -> {
                    ErrorHandler.handlerErrorPresenter(getMvpView(),throwable);
                });
    }
}