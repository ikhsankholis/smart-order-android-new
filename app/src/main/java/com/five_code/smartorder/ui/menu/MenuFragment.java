package com.five_code.smartorder.ui.menu;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseFragment;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.injection.component.ActivityComponent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class MenuFragment extends BaseFragment implements MenuView {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    private MenuAdapter menuAdapter;

    @Inject MenuPresenter mMenuPresenter;

    private List<Menu> menuList = new ArrayList<>();

    private int position;

    private HomeView homeView;

    public static MenuFragment newInstance(int position) {

        Bundle args = new Bundle();

        MenuFragment fragment = new MenuFragment();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayout() {

        return R.layout.fragment_menu;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        menuAdapter = new MenuAdapter(mActivity);
        rvContent.setLayoutManager(new GridLayoutManager(mActivity, 4));
        rvContent.setAdapter(menuAdapter);

        mMenuPresenter.attachView(this);
        mMenuPresenter.getMenu();

        position = getArguments().getInt("position", 0);

        menuAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Dialog dialog = new Dialog(mActivity);
                dialog.setContentView(R.layout.dialog_add_cart);

                dialog.setCancelable(false);
                dialog.setTitle(menuAdapter.getData().get(position).getNama());

                EditText editTxtQty = (EditText) dialog.findViewById(R.id.edit_txt_qty);
                EditText editTxtNote = (EditText) dialog.findViewById(R.id.edit_txt_note);
                Button btnDialogOk = (Button) dialog.findViewById(R.id.btn_dialog_ok);
                btnDialogOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Menu menu = menuAdapter.getData().get(position);
                        Cart cart = new Cart();
                        cart.set_id(menu.get_id());
                        cart.setNama(menu.getNama());
                        cart.setHarga(menu.getHarga());

                        String quantity = editTxtQty.getText().toString();
                        String note = editTxtNote.getText().toString();
                        if (quantity.isEmpty())
                            return;
                        cart.setQuantity(Integer.parseInt(quantity));
                        cart.setNote(note);

                        homeView.addToCart(cart);
                        dialog.dismiss();
                    }
                });

                ButterKnife.findById(dialog, R.id.btn_dialog_cancel).setOnClickListener(view1 -> {
                    dialog.cancel();
                });

                dialog.show();
            }
        });

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onDismissLoading() {

    }

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void onSuccessTable(List<Menu> menus) {
        menuAdapter.clear();
        menuList.clear();

        for (Menu menu : menus) {
            if (position == 1) {
                if (menu.getKategori_id().getKategori().equals("Drink"))
                    menuList.add(menu);
            } else if (position == 0) {
                if (menu.getKategori_id().getKategori().equals("Food"))
                    menuList.add(menu);
            } else if (position == 2) {
                if (menu.getKategori_id().getKategori().equals("Dessert"))
                    menuList.add(menu);
            } else if (position == 3) {
                if (menu.getKategori_id().getKategori().equals("Snack"))
                    menuList.add(menu);
            }
        }

        menuAdapter.addAll(menuList);
    }

    public void setListener(HomeView homeView) {

        this.homeView = homeView;
    }

}
