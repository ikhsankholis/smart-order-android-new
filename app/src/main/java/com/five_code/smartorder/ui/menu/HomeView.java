package com.five_code.smartorder.ui.menu;

import com.five_code.smartorder.base.MvpView;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.Menu;

/**
 * Created by IKHSAN on 6/11/2017.
 */

public interface HomeView {

    void addToCart(Cart cart);
}
