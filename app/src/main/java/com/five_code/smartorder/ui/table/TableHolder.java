package com.five_code.smartorder.ui.table;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseItemRecyclerViewHolder;
import com.five_code.smartorder.data.model.Table;

import butterknife.BindView;

/**
 * Created by IKHSAN on 5/16/2017.
 */

public class TableHolder extends BaseItemRecyclerViewHolder<Table> {

    @BindView(R.id.text_meja) TextView textMeja;
    @BindView(R.id.status_meja) TextView statusMeja;

    public TableHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
    }

    @Override
    public void bind(Table table) {
        String status = table.getMeja_status().equals("0") ? "Unavailable" : "Available";

        textMeja.setText(table.getMeja_no());
        statusMeja.setText(status);
    }
}
