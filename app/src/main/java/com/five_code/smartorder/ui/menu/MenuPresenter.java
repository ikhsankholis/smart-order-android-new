package com.five_code.smartorder.ui.menu;

import com.five_code.smartorder.base.BasePresenter;
import com.five_code.smartorder.data.DataManager;
import com.five_code.smartorder.utils.ErrorHandler;
import com.five_code.smartorder.utils.RxUtils;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by IKHSAN on 6/6/2017.
 */

public class MenuPresenter extends BasePresenter<MenuView> {

    @Inject
    public MenuPresenter (DataManager dataManager) { mDataManager = dataManager; }

    @Override
    public void attachView(MenuView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void getMenu(){
        checkViewAttached();
        RxUtils.unsubscribe(mSubscription);

        getMvpView().onShowLoading();
        mSubscription = mDataManager.getMenu()
                .doOnTerminate(()-> getMvpView().onDismissLoading())
                .compose(RxUtils.applyApiCall())
                .subscribe(menuList -> {
                    getMvpView().onSuccessTable(menuList);
                }, throwable -> {
                    ErrorHandler.handlerErrorPresenter(getMvpView(),throwable);
                });
    }
}
