package com.five_code.smartorder.ui.cart;

import android.content.Context;
import android.view.ViewGroup;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.Menu;

/**
 * Created by IKHSAN on 5/18/2017.
 */

public class CartAdapter extends BaseAdapterItemReyclerView<Cart, CartHolder> {
    public CartAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_cart;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartHolder(context, getView(parent, viewType),itemClickListener, longItemClickListener);
    }

}
