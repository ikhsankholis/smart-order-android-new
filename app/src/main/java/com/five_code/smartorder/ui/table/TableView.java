package com.five_code.smartorder.ui.table;

import com.five_code.smartorder.base.MvpView;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Table;

import java.util.List;

/**
 * Created by IKHSAN on 6/1/2017.
 */

public interface TableView extends MvpView {

    void onSuccessTable(List<Table>tableList);

    void onSuccessDirectOrder(DirectOrderResponse directOrderResponse);
}
