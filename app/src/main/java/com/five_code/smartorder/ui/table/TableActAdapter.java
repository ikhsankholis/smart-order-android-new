package com.five_code.smartorder.ui.table;

import android.content.Context;
import android.view.ViewGroup;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.data.model.Table;

/**
 * Created by IKHSAN on 5/16/2017.
 */

public class TableActAdapter extends BaseAdapterItemReyclerView<Table, TableHolder>{

    public TableActAdapter(Context context) {

        super(context);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {

        return R.layout.card_view_table;
    }

    @Override
    public TableHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TableHolder(context, getView(parent,viewType),itemClickListener, longItemClickListener);
    }
}
