package com.five_code.smartorder.ui.cart;

import com.five_code.smartorder.base.MvpView;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.data.model.OrderResponse;
import com.five_code.smartorder.data.model.PostOrder;

import java.util.List;

import retrofit2.http.Body;
import rx.Observable;

/**
 * Created by IKHSAN on 6/15/2017.
 */

public interface CartView extends MvpView {

    void onSuccessOrder(OrderResponse orderResponse);
}
