package com.five_code.smartorder.ui.cart;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseItemRecyclerViewHolder;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.utils.Utils;

import butterknife.BindView;

/**
 * Created by IKHSAN on 5/18/2017.
 */

public class CartHolder extends BaseItemRecyclerViewHolder<Cart> {

    @BindView(R.id.txt_nama_pesanan) TextView txtNamaPesanan;
    @BindView(R.id.txt_qty) TextView txtQty;
    @BindView(R.id.txt_harga) TextView txtHarga;
    @BindView(R.id.card_view_cart) CardView cardViewCart;
    @BindView(R.id.txt_note) TextView txtNote;

    public CartHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
    }

    @Override
    public void bind(Cart cart) {
        txtNamaPesanan.setText(cart.getNama());
        txtQty.setText(cart.getQuantity() + " pcs");
        txtHarga.setText(Utils.convertNumber(cart.getQuantity() * Integer.parseInt(cart.getHarga())));

        txtNote.setText(cart.getNote());
        if (cart.getNote().isEmpty())
            txtNote.setVisibility(View.GONE);
        else
            txtNote.setVisibility(View.VISIBLE);
    }

}
