package com.five_code.smartorder.ui.menu;

import com.five_code.smartorder.base.MvpView;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.data.model.Table;

import java.util.List;

/**
 * Created by IKHSAN on 6/6/2017.
 */

public interface MenuView extends MvpView {

    void onSuccessTable(List<Menu> menuList);
}
