package com.five_code.smartorder.ui.cart;

import android.app.Dialog;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.base.BaseFragment;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.data.model.OrderResponse;
import com.five_code.smartorder.data.model.PostOrder;
import com.five_code.smartorder.ui.menu.MenuFragment;
import com.five_code.smartorder.utils.ErrorHandler;
import com.five_code.smartorder.utils.MessageDialog;
import com.five_code.smartorder.utils.ProgressDialog;
import com.five_code.smartorder.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by IKHSAN on 5/18/2017.
 */

public class CartFragment extends BaseFragment implements CartView {

    @Inject CartPresenter cartPresenter;

    @BindView(R.id.rv_cart) RecyclerView rvCart;
    @BindView(R.id.lin_price) LinearLayout linPrice;
    @BindView(R.id.text_price) TextView textPrice;

    private CartAdapter cartAdapter;
    private List<Cart> cartList = new ArrayList<>();

    private DirectOrderResponse.DirectordersBean directordersBean;

    private ProgressDialog mProgressDialog;

    public static CartFragment newInstance(DirectOrderResponse.DirectordersBean directordersBean) {

        Bundle args = new Bundle();

        CartFragment fragment = new CartFragment();
        args.putSerializable(DirectOrderResponse.DirectordersBean.class.getSimpleName(), directordersBean);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_cart;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cartAdapter = new CartAdapter(mActivity);
        rvCart.setLayoutManager(new LinearLayoutManager(mActivity));
        rvCart.setAdapter(cartAdapter);

        directordersBean = (DirectOrderResponse.DirectordersBean)
                getArguments().getSerializable(DirectOrderResponse.DirectordersBean.class.getSimpleName());

        cartAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Dialog dialog = new Dialog(mActivity);
                dialog.setContentView(R.layout.dialog_pesan);

                dialog.setCancelable(false);
                dialog.setTitle(cartAdapter.getData().get(position).getNama());

                EditText editTxtPesan = (EditText) dialog.findViewById(R.id.edit_txt_pesan);
                EditText editTxtNote = (EditText) dialog.findViewById(R.id.edit_txt_note);
                editTxtPesan.setText(cartAdapter.getData().get(position).getQuantity() + "");
                editTxtNote.setText(cartAdapter.getData().get(position).getNote());

                ButterKnife.findById(dialog, R.id.img_minus).setOnClickListener(view1 -> {
                    int quantity = Integer.parseInt(editTxtPesan.getText().toString());
                    if (quantity == 1)
                        return;
                    int newQuantity = quantity - 1;
                    editTxtPesan.setText(newQuantity + "");
                });

                ButterKnife.findById(dialog, R.id.img_plus).setOnClickListener(view1 -> {
                    int quantity = Integer.parseInt(editTxtPesan.getText().toString());
                    int newQuantity = quantity + 1;
                    editTxtPesan.setText(newQuantity + "");
                });

                ButterKnife.findById(dialog, R.id.btn_dialog_cancel).setOnClickListener(view1 -> {
                    dialog.cancel();
                });

                ButterKnife.findById(dialog, R.id.btn_dialog_delete).setOnClickListener(view1 -> {
                    cartList.remove(position);
                    cartAdapter.clear();
                    cartAdapter.addAll(cartList);
                    dialog.cancel();
                });

                ButterKnife.findById(dialog, R.id.btn_dialog_order).setOnClickListener(view1 -> {
                    cartList.get(position).setQuantity(Integer.parseInt(editTxtPesan.getText().toString()));
                    cartAdapter.clear();
                    cartAdapter.addAll(cartList);
                    dialog.cancel();
                });

                dialog.show();
            }
        });

        cartPresenter.attachView(this);
    }

    public void addMenu(Cart cart) {
        List<Cart> menuList = new ArrayList<>();
        boolean statusMenu = false;

        menuList.addAll(cartAdapter.getData());

        for (int i = 0; i < menuList.size(); i++) {
            Cart menuCart = menuList.get(i);
            if (menuCart.get_id().equals(cart.get_id())) {
                menuList.get(i).setQuantity(menuCart.getQuantity() + cart.getQuantity());
                menuList.get(i).setNote(cart.getNote());
                statusMenu = true;
                break;
            }
        }

        if (statusMenu) {
            cartAdapter.clear();
            cartAdapter.addAll(menuList);
        } else {
            cartAdapter.add(cart);
        }

        cartList.clear();
        cartList.addAll(cartAdapter.getData());

        linPrice.setVisibility(View.VISIBLE);
        int total = 0;
        for (Cart myCart : cartAdapter.getData()) {
            total += Integer.parseInt(myCart.getHarga()) * myCart.getQuantity();
        }
        textPrice.setText(Utils.convertNumber(total));
    }

    @OnClick(R.id.btn_pesan)
    void onClickPesan() {
        List<PostOrder.ArrayBean> arrayBeanList = new ArrayList<>();
        for (Cart cart : cartAdapter.getData()) {
            int subTotal = cart.getQuantity() * Integer.parseInt(cart.getHarga());
            PostOrder.ArrayBean arrayBean = new PostOrder.ArrayBean(directordersBean.get_id() + "",
                    cart.get_id() + "", cart.getQuantity() + "", subTotal + "", "0", cart.getNote());
            arrayBeanList.add(arrayBean);
        }
        PostOrder postOrder = new PostOrder(arrayBeanList);
        cartPresenter.postOrder(postOrder);
    }

    @Override
    public void onShowLoading() {
        mProgressDialog = ProgressDialog.create();
        mProgressDialog.show(getChildFragmentManager());
    }

    @Override
    public void onDismissLoading() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onFailed(String message) {
        MessageDialog.showMessage(getActivity(), message);
    }

    @Override
    public void onSuccessOrder(OrderResponse orderResponse) {
        cartAdapter.clear();
        linPrice.setVisibility(View.GONE);
        MessageDialog.showMessage(getActivity(), "Success Order");
    }
}