package com.five_code.smartorder.ui.menu;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseActivity;
import com.five_code.smartorder.base.BaseFragment;
import com.five_code.smartorder.data.model.Cart;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.Menu;
import com.five_code.smartorder.ui.cart.CartFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IKHSAN on 5/17/2017.
 */

public class HomeAct extends BaseActivity implements HomeView {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private MenuFragment foodFragment;
    private MenuFragment drinkFragment;
    private MenuFragment snackFragment;
    private MenuFragment dessertFragment;
    private CartFragment cartFragment;

    private DirectOrderResponse.DirectordersBean directordersBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        directordersBean = (DirectOrderResponse.DirectordersBean)
                getIntent().getSerializableExtra(DirectOrderResponse.DirectordersBean.class.getSimpleName());

        cartFragment = CartFragment.newInstance(directordersBean);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, cartFragment).commit();

        foodFragment = MenuFragment.newInstance(0);
        drinkFragment = MenuFragment.newInstance(1);
        dessertFragment = MenuFragment.newInstance(2);
        snackFragment = MenuFragment.newInstance(3);

        foodFragment.setListener(this);
        drinkFragment.setListener(this);

        setupViewpager();
    }

    public void setupViewpager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(foodFragment, "Makanan");
        adapter.addFragment(drinkFragment, "Minuman");
        adapter.addFragment(dessertFragment, "Dessert");
        adapter.addFragment(snackFragment, "Snack");

        class ViewPagerAdapter extends FragmentPagerAdapter {
            private final List<Fragment> mFragmentList = new ArrayList<>();
            private final List<String> mFragmentTitleList = new ArrayList<>();

            public ViewPagerAdapter(FragmentManager manager) {
                super(manager);
            }

            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }

            public void addFragment(Fragment fragment, String title) {
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mFragmentTitleList.get(position);
            }

        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void addToCart(Cart cart) {
        cartFragment.addMenu(cart);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

}
