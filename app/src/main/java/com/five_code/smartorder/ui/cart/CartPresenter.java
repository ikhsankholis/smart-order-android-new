package com.five_code.smartorder.ui.cart;

import com.five_code.smartorder.base.BasePresenter;
import com.five_code.smartorder.data.DataManager;
import com.five_code.smartorder.data.model.PostOrder;
import com.five_code.smartorder.utils.ErrorHandler;
import com.five_code.smartorder.utils.RxUtils;

import javax.inject.Inject;

/**
 * Created by IKHSAN on 6/15/2017.
 */

public class CartPresenter extends BasePresenter<CartView> {

    @Inject
    public CartPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(CartView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void postOrder(PostOrder postOrder) {
        checkViewAttached();
        RxUtils.unsubscribe(mSubscription);

        getMvpView().onShowLoading();
        mSubscription = mDataManager.postOrder(postOrder)
                .doOnTerminate(() -> getMvpView().onDismissLoading())
                .compose(RxUtils.applyApiCall())
                .subscribe(orderResponse -> {
                    getMvpView().onSuccessOrder(orderResponse);
                }, throwable -> {
                    ErrorHandler.handlerErrorPresenter(getMvpView(), throwable);
                });

    }
}
