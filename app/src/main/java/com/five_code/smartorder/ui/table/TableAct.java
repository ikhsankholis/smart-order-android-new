package com.five_code.smartorder.ui.table;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.five_code.smartorder.base.BaseActivity;
import com.five_code.smartorder.R;
import com.five_code.smartorder.base.BaseAdapterItemReyclerView;
import com.five_code.smartorder.data.model.DirectOrderResponse;
import com.five_code.smartorder.data.model.PostDirectOrder;
import com.five_code.smartorder.data.model.Table;
import com.five_code.smartorder.ui.menu.HomeAct;
import com.five_code.smartorder.utils.MessageDialog;
import com.five_code.smartorder.utils.ProgressDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IKHSAN on 5/16/2017.
 */

public class TableAct extends BaseActivity implements TableView, BaseAdapterItemReyclerView.OnItemClickListener {

    @BindView(R.id.rv_table)
    RecyclerView rvTable;

    @Inject TablePresenter mTablePresenter;

    private TableActAdapter tableActAdapter;

    private int positionSelected = 0;

    private ProgressDialog mProgressDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        ButterKnife.bind(this);

        activityComponent().inject(this);

        tableActAdapter = new TableActAdapter(this);
        rvTable.setLayoutManager(new GridLayoutManager(this, 7));
        rvTable.setAdapter(tableActAdapter);
        tableActAdapter.setOnItemClickListener(this);

        mTablePresenter.attachView(this);
        mTablePresenter.getTable();
    }

    @Override
    public void onShowLoading() {
        mProgressDialog = ProgressDialog.create();
        mProgressDialog.show(getBaseFragmentManager());
    }

    @Override
    public void onDismissLoading() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onFailed(String message) {
        MessageDialog.showMessage(this , message);
    }

    @Override
    public void onSuccessTable(List<Table> tableList) {
        tableActAdapter.clear();
        tableActAdapter.addAll(tableList);
    }

    @Override
    public void onSuccessDirectOrder(DirectOrderResponse directOrderResponse) {
        tableActAdapter.getData().get(positionSelected).setMeja_status("0");
        tableActAdapter.notifyDataSetChanged();

        startActivity(new Intent(this, HomeAct.class)
                .putExtra(DirectOrderResponse.DirectordersBean.class.getSimpleName(), directOrderResponse.getDirectorders()));
    }

    @Override
    public void onItemClick(View view, int position) {
        positionSelected = position;
        PostDirectOrder postDirectOrder = new PostDirectOrder(tableActAdapter.getData().get(position).get_id(),
                tableActAdapter.getData().get(position).getMeja_status());
        mTablePresenter.postDirectOrder(postDirectOrder);
    }
}